var timer;

function startIt() {
 timer = setInterval(function () {
        var d = new Date();
        var n = d.toDateString() + " " + d.toTimeString();
        document.getElementById("date").textContent = n;
    },100)
};

document.addEventListener("DOMContentLoaded", function () {
    startIt();
    document.getElementById("stopper").addEventListener("click", function () {
        console.log("button pushed");
        clearInterval(timer);
    });
    document.getElementById("continue").addEventListener("click", function () {
        console.log("continue pushed");
        startIt();

    });

});

