$(document).ready(function () {
    //turn the pages
    var pageNumber = 0;


    $(".page").on('click', function () {
        $(this).addClass('pageAnimation').css('animation-delay', '0s');
        pageNumber += 1;
        window.setTimeout(changeZ, 400);

        function changeZ() {
            $('#page' + pageNumber).css('z-index', 1);
        }

    });

});

