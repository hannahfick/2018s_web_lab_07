$(document).ready(function () {


        var numFemale = 0;
        var numMale = 0;
        var youngRange = 0;
        var olderRange = 0;
        var oldestRange = 0;
        var silver = 0;
        var gold = 0;
        var bronze = 0;


//figuring out how many males and females there are
        for (let i = 0; i < customers.length; i++) {


            if (customers[i].gender === "female") {
                numFemale++;
            }
            else if (customers[i].gender === "male") {
                numMale++;
            }
        }
// getting the age ranges
        for (let i = 0; i < customers.length; i++) {


            if ((2018 - customers[i].year_born) < 31) {
                youngRange++;
            }
            else if ((2018 - customers[i].year_born) < 65) {
                olderRange++;
            }
            else if ((2018 - customers[i].year_born) > 64) {
                oldestRange++;
            }

        }

        for (let i = 0; i < customers.length; i++) {

            let n = customers[i].num_hires / ((2018.0 - customers[i].joined) * 52);

            if (n > 4) {
                gold++;
            }
            if ((4 >= n && n >= 1)) {
                silver++;
            }
            if (n < 1) {
                bronze++;
            }

        }

        // console.log(youngRange, olderRange, oldestRange);
        // console.log(numFemale, numMale);


        //creating the table here
        var table = document.getElementById("contents");


        for (let i = 0; i < customers.length; i++) {
            var row = table.insertRow(0);

            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            var cell3 = row.insertCell(2);
            var cell4 = row.insertCell(3);
            var cell5 = row.insertCell(4);

            cell1.textContent = customers[i].name;
            cell2.textContent = customers[i].gender;
            cell3.textContent = customers[i].year_born;
            cell4.textContent = customers[i].joined;
            cell5.textContent = customers[i].num_hires;
        }
        row = table.insertRow();

        cell1 = row.insertCell();
        cell1.textContent = "Number of Females";
        cell2 = row.insertCell();
        cell2.textContent = numFemale;
        cell2.setAttribute("colspan", "4");

        row = table.insertRow();

        cell1 = row.insertCell();
        cell1.textContent = "Number of Males";
        cell2 = row.insertCell();
        cell2.textContent = numMale;
        cell2.setAttribute("colspan", "4");

        row = table.insertRow();

        cell1 = row.insertCell();
        cell1.textContent = "Age 0-31";
        cell2 = row.insertCell();
        cell2.textContent = youngRange;
        cell2.setAttribute("colspan", "4");

        row = table.insertRow();

        cell1 = row.insertCell();
        cell1.textContent = "Age 31-64";
        cell2 = row.insertCell();
        cell2.textContent = olderRange;
        cell2.setAttribute("colspan", "4");


        row = table.insertRow();

        cell1 = row.insertCell();
        cell1.textContent = "Age 65+";
        cell2 = row.insertCell();
        cell2.textContent = oldestRange;
        cell2.setAttribute("colspan", "4");

        row = table.insertRow();

        cell1 = row.insertCell();
        cell1.textContent = "Gold";
        cell2 = row.insertCell();
        cell2.textContent = gold;
        cell2.setAttribute("colspan", "4");

        row = table.insertRow();

        cell1 = row.insertCell();
        cell1.textContent = "Silver";
        cell2 = row.insertCell();
        cell2.textContent = silver;
        cell2.setAttribute("colspan", "4");

        row = table.insertRow();

        cell1 = row.insertCell();
        cell1.textContent = "Bronze";
        cell2 = row.insertCell();
        cell2.textContent = bronze;
        cell2.setAttribute("colspan", "4");


    }
);